console.log("Hello World");

// JSON
// JSON or Javascript Object Notation is a popular data format for application to communicate with each other

// JSON may look like a Javascript Object, but it is actually a String

/*
	JSON Syntax

	{
		"key1": valueA,
		"key2": valueB
	}

	Keys are wrapped in curly brace
*/

let sample1 = `
	{
		"name": "Katniss Everdeen",
		"age": 20,
		"address": {
			"city": "Kansas City",
			"state": "Kansas"
		}
	}
`;

console.log(sample1);

// JSON.parse() - will return the JSON as a JS Object
// JSON.parse() does not mutate or update the original JSON

console.log(JSON.parse(sample1));

// JSON Array

let sampleArr = `
	[
		{
			"email": "jasonNested@gmail.com",
			"password": "iplaybass1234",
			"isAdmin": false
		},
		{
			"email": "larsDrums@gmail.com",
			"password": "metallicaMe80",
			"isAdmin": true
		}
	]
`;

console.log(sampleArr);

// We can't use Array Mathods on JSON array unless we convert it to JS array first using JSON.parse()

let parsedSampleArr = JSON.parse(sampleArr);
console.log(parsedSampleArr);

console.log(parsedSampleArr.pop());
console.log(parsedSampleArr);

// If for example, we need to send this data back to the client/front end, it should be in JSON format

//JSON.stringify() - will stringify JS objects as JSON

sampleArr = JSON.stringify(parsedSampleArr);
console.log(sampleArr);

// Database => Server/API (JSON to JS object to process) => sent as JSON => client

let jsonArr = `
	[
	 
		"pizza",
		"hamburger",
		"spaghetti",
		"shanghai",
		"hotdog stick on a pineapple",
		"pancit bihon"
	 
	]
`;

console.log(jsonArr);
let parsedJsonArr = JSON.parse(jsonArr);
console.log(parsedJsonArr.pop());
(parsedJsonArr.push("barbeque"));
console.log(parsedJsonArr);
jsonArr = JSON.stringify(parsedJsonArr);
console.log(jsonArr);